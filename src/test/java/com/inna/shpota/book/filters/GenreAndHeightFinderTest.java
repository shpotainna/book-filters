package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class GenreAndHeightFinderTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldFind() {
        GenreAndHeightFinder genreAndHeightFinder = new GenreAndHeightFinder(books(), "genre", 11);
        List<Book> expected = expected();

        List<Book> actual = genreAndHeightFinder.find();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldFailToConstructorGivenNullList() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Book list must not be null.");

        new GenreAndHeightFinder(null, "genre", 10);
    }

    @Test
    public void shouldFailToConstructorGivenNullGenre() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Genre must not be null.");

        new GenreAndHeightFinder(books(), null, 10);
    }

    @Test
    public void shouldFailToConstructorGivenNullHeight() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Height must be positive.");

        new GenreAndHeightFinder(books(), "genre", -10);
    }

    private List<Book> books() {
        return asList(
                Book.builder()
                        .genre("genre")
                        .height(11)
                        .build(),
                Book.builder()
                        .genre("genre1")
                        .height(11)
                        .build(),
                Book.builder()
                        .genre("genre")
                        .height(12)
                        .build(),
                Book.builder()
                        .genre("genre")
                        .height(11)
                        .build()
        );
    }

    private List<Book> expected() {
        return asList(
                Book.builder()
                        .genre("genre")
                        .height(11)
                        .build(),
                Book.builder()
                        .genre("genre")
                        .height(11)
                        .build()
        );
    }
}