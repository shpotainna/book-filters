package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AuthorCheckTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldBeAuthorTrue() {
        AuthorCheck authorCheck = new AuthorCheck(books(), "author");

        boolean actual = authorCheck.isAuthor();

        assertTrue(actual);
    }

    @Test
    public void shouldBeAuthorFalse() {
        AuthorCheck authorCheck = new AuthorCheck(books(), "nnn");

        boolean actual = authorCheck.isAuthor();

        assertFalse(actual);
    }

    @Test
    public void shouldFailToConstructorGivenNullList() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Book list must not be null.");

        new AuthorCheck(null, "author");
    }

    @Test
    public void shouldFailToConstructorGivenNullAuthor() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Author must not be null.");

        new AuthorCheck(books(), null);
    }

    private List<Book> books() {
        return singletonList(
                Book.builder()
                        .author("author")
                        .build()
        );
    }
}
