package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class GenreCounterTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldCount() {
        GenreCounter genreCounter = new GenreCounter(books(), "genre");

        long actual = genreCounter.count();

        assertEquals(2, actual);
    }

    @Test
    public void shouldFailToConstructorGivenNullList() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Book list must not be null.");

        new GenreCounter(null, "genre");
    }

    @Test
    public void shouldFailToConstructorGivenNullAuthor() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Genre must not be null.");

        new GenreCounter(books(), null);
    }

    private List<Book> books() {
        return asList(
                Book.builder()
                        .genre("genre")
                        .build(),
                Book.builder()
                        .genre("genre1")
                        .build(),
                Book.builder()
                        .genre("genre")
                        .build()
        );
    }
}