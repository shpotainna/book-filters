package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class AuthorPublisherCounterTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldCount() {
        AuthorPublisherCounter authorPublisherCounter = new AuthorPublisherCounter(books(), "publisher");

        long actual = authorPublisherCounter.count();

        assertEquals(2, actual);
    }

    @Test
    public void shouldFailToConstructorGivenNullList() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Book list must not be null.");

        new AuthorPublisherCounter(null, "publisher");
    }

    @Test
    public void shouldFailToConstructorGivenNullPublisher() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Publisher must not be null.");

        new AuthorPublisherCounter(books(), null);
    }

    private List<Book> books() {
        return asList(
                Book.builder()
                        .author("author1")
                        .publisher("publisher")
                        .build(),
                Book.builder()
                        .author("author1")
                        .publisher("publisher")
                        .build(),
                Book.builder()
                        .author("author2")
                        .publisher("publisher")
                        .build()
        );
    }
}