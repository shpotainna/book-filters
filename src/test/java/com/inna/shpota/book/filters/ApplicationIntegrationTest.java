package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;
import com.inna.shpota.book.CSVReader;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class ApplicationIntegrationTest {
    private List<Book> books;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void before() throws IOException {
        CSVReader csvReader = new CSVReader();
        books = csvReader.loadFile();
    }

    @Test
    public void shouldBeSingleGenreTrue() {
        AuthorWithGenreCheck authorWithGenreCheck = new AuthorWithGenreCheck(books, "Dalrymple, William");

        boolean actual = authorWithGenreCheck.isSingleGenre();

        assertTrue(actual);
    }

    @Test
    public void shouldBeSingleGenreFalse() {
        AuthorWithGenreCheck authorWithGenreCheck = new AuthorWithGenreCheck(books, "Hawking, Stephen");

        boolean actual = authorWithGenreCheck.isSingleGenre();

        assertFalse(actual);
    }

    @Test
    public void shouldFailToConstructorGivenNullList() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Book list must not be null.");

        new AuthorWithGenreCheck(null, "author");
    }

    @Test
    public void shouldFailToConstructorGivenNullAuthor() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Author must not be null.");

        new AuthorWithGenreCheck(books, null);
    }
}