package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class AuthorCounterTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldCount() {
        AuthorCounter authorCounter = new AuthorCounter(books(), "author1");

        long actual = authorCounter.count();

        assertEquals(2, actual);
    }

    @Test
    public void shouldFailToConstructorGivenNullList() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Book list must not be null.");

        new AuthorCounter(null, "author");
    }

    @Test
    public void shouldFailToConstructorGivenNullAuthor() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Author must not be null.");

        new AuthorCounter(books(), null);
    }

    private List<Book> books() {
        return asList(
                Book.builder()
                        .author("author1")
                        .build(),
                Book.builder()
                        .author("author1")
                        .build(),
                Book.builder()
                        .author("author2")
                        .build()
        );
    }
}