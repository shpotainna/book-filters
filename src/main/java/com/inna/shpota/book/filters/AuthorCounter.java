package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;

import java.util.List;

public class AuthorCounter {
    private List<Book> books;
    private String author;

    AuthorCounter(List<Book> books, String author) {
        if (books == null) {
            throw new IllegalArgumentException("Book list must not be null.");
        }
        if (author == null) {
            throw new IllegalArgumentException("Author must not be null.");
        }
        this.books = books;
        this.author = author;
    }

    public long count() {
        return books.stream()
                .map(Book::getAuthor)
                .filter(author::equals)
                .count();
    }
}
