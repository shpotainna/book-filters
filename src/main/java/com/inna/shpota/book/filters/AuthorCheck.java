package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;

import java.util.List;

public class AuthorCheck {
    private List<Book> books;
    private String author;

    AuthorCheck(List<Book> books, String author) {
        if (books == null) {
            throw new IllegalArgumentException("Book list must not be null.");
        }
        if (author == null) {
            throw new IllegalArgumentException("Author must not be null.");
        }
        this.books = books;
        this.author = author;
    }

    public boolean isAuthor() {
        return books.stream()
                .map(Book::getAuthor)
                .anyMatch(author::equals);
    }
}
