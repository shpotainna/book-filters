package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;

import java.util.List;

public class AuthorPublisherCounter {
    private List<Book> books;
    private String publisher;

    AuthorPublisherCounter(List<Book> books, String publisher) {
        if (books == null) {
            throw new IllegalArgumentException("Book list must not be null.");
        }
        if (publisher == null) {
            throw new IllegalArgumentException("Publisher must not be null.");
        }
        this.books = books;
        this.publisher = publisher;
    }

    public long count() {
        return books.stream()
                .filter(book -> publisher.equals(book.getPublisher()))
                .map(Book::getAuthor)
                .distinct()
                .count();
    }
}
