package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;

import java.util.List;

public class GenreCounter {
    private List<Book> books;
    private String genre;

    GenreCounter(List<Book> books, String genre) {
        if (books == null) {
            throw new IllegalArgumentException("Book list must not be null.");
        }
        if (genre == null) {
            throw new IllegalArgumentException("Genre must not be null.");
        }
        this.books = books;
        this.genre = genre;
    }

    public long count() {
        return books.stream()
                .map(Book::getGenre)
                .filter(genre::equals)
                .count();
    }
}