package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;

import java.util.List;

public class AuthorWithGenreCheck {
    private List<Book> books;
    private String author;

    AuthorWithGenreCheck(List<Book> books, String author) {
        if (books == null) {
            throw new IllegalArgumentException("Book list must not be null.");
        }
        if (author == null) {
            throw new IllegalArgumentException("Author must not be null.");
        }
        this.books = books;
        this.author = author;
    }

    public boolean isSingleGenre() {
        long count = books.stream()
                .filter(book -> author.equals(book.getAuthor()))
                .map(Book::getGenre)
                .distinct()
                .count();
        return count == 1;
    }
}
