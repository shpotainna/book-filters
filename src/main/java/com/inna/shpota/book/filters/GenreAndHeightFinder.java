package com.inna.shpota.book.filters;

import com.inna.shpota.book.Book;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class GenreAndHeightFinder {
    private List<Book> books;
    private String genre;
    private int height;

    GenreAndHeightFinder(List<Book> books, String genre, int height) {
        if (books == null) {
            throw new IllegalArgumentException("Book list must not be null.");
        }
        if (genre == null) {
            throw new IllegalArgumentException("Genre must not be null.");
        }
        if (height <= 0) {
            throw new IllegalArgumentException("Height must be positive.");
        }
        this.books = books;
        this.genre = genre;
        this.height = height;
    }

    public List<Book> find() {
        return books.stream()
                .filter(book -> genre.equals(book.getGenre()))
                .filter(book -> height == book.getHeight())
                .collect(toList());
    }
}
