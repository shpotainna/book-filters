package com.inna.shpota.book;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {
    private static final String CSV_FILE_PATH = "/booksList.csv";

    public List<Book> loadFile() throws IOException {
        InputStream inputStream = this.getClass().getResourceAsStream(CSV_FILE_PATH);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        try (Reader reader = new BufferedReader(inputStreamReader);
             CSVParser csvParser = new CSVParser(
                     reader,
                     CSVFormat.DEFAULT
                             .withHeader("title", "author", "genre", "height", "publisher")
                             .withIgnoreHeaderCase()
                             .withTrim()
                             .withSkipHeaderRecord())
        ) {
            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            List<Book> books = new ArrayList<>();
            for (CSVRecord csvRecord : csvRecords) {
                String title = csvRecord.get("Title");
                String author = csvRecord.get("Author");
                String genre = csvRecord.get("Genre");
                int height = Integer.parseInt(csvRecord.get("Height"));
                String publisher = csvRecord.get("Publisher");
                books.add(Book.builder()
                        .title(title)
                        .author(author)
                        .genre(genre)
                        .height(height)
                        .publisher(publisher)
                        .build());
            }
            return books;
        }
    }
}
