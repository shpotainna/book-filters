package com.inna.shpota.book;

import java.util.Objects;

public class Book {
    private final String title;
    private final String author;
    private final String genre;
    private final int height;
    private final String publisher;

    private Book(String title, String author, String genre, int height, String publisher) {
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.height = height;
        this.publisher = publisher;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getGenre() {
        return genre;
    }

    public int getHeight() {
        return height;
    }

    public String getPublisher() {
        return publisher;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return height == book.height &&
                Objects.equals(title, book.title) &&
                Objects.equals(author, book.author) &&
                Objects.equals(genre, book.genre) &&
                Objects.equals(publisher, book.publisher);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, genre, height, publisher);
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", genre='" + genre + '\'' +
                ", height=" + height +
                ", publisher='" + publisher + '\'' +
                '}';
    }

    public static class Builder {
        private String title;
        private String author;
        private String genre;
        private int height;
        private String publisher;

        private Builder() { }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder genre(String genre) {
            this.genre = genre;
            return this;
        }

        public Builder height(int height) {
            this.height = height;
            return this;
        }

        public Builder publisher(String publisher) {
            this.publisher = publisher;
            return this;
        }

        public Book build() {
            return new Book(title, author, genre, height, publisher);
        }
    }
}
