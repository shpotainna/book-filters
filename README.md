# Stream API #

Використовуючи stream api написати такі методи:

- [метод](./src/main/java/com/inna/shpota/book/filters/AuthorCheck.java),
 який визначає наявність книги за вказаним автором
- [метод](./src/main/java/com/inna/shpota/book/filters/AuthorCounter.java),
 який визначає кількість книг, написаних зазначеним автором
- [метод](./src/main/java/com/inna/shpota/book/filters/GenreCounter.java), 
 який визначає кількість книг, написаних в зазначеному жанрі
- [метод](./src/main/java/com/inna/shpota/book/filters/AuthorPublisherCounter.java),
 який визначає кількість авторів, опублікованих зазначеним видавництвом
- [метод](./src/main/java/com/inna/shpota/book/filters/GenreAndHeightFinder.java),
 який робить пошук книг за жанром і розміром (колонка height)
- [метод](./src/main/java/com/inna/shpota/book/filters/AuthorWithGenreCheck.java),
 який перевіряє чи писав вказаний автор в одному жанрі, чи в декількох
 
 
[Тести](./src/test/java/com/inna/shpota/book/filters)